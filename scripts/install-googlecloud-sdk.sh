#Creado por Heberth Ardila 
#heberthardila@gmail.com
#Version 0.1

# Basado en la documentacion del desarrollador 
# https://cloud.google.com/sdk/docs/quickstart-debian-ubuntu

#Informacion de instalacion.
echo "instalaremos el SDK de Google Cloud sobre nuestra distribucion Debian/Ubuntu"
echo .
sleep 1
echo ..
sleep 1
echo ...
sleep 1

#Crear la variable de entorno adecuada para la distribucion
export CLOUD_SDK_REPO="cloud-sdk-$(lsb_release -c -s)"
echo "Repositorio seleccionado: $CLOUD_SDK_REPO"


#Agregar la URL de los paquetes del SDK a nuestro source sobre el directorio /etc/apt/sources.list.d
echo "deb http://packages.cloud.google.com/apt $CLOUD_SDK_REPO main" | sudo tee -a /etc/apt/sources.list.d/google-cloud-sdk.list

# Importamos la llave publica del servidor para validar la descarga de los paquetes
curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -

#Instalamos el paquete
sudo apt update && sudo apt install google-cloud-sdk -y

echo "Instalacion correcta :)"




